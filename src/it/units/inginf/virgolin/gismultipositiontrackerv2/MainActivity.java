package it.units.inginf.virgolin.gismultipositiontrackerv2;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import it.units.inginf.virgolin.gismultipositiontrackerv2.R;
import it.units.inginf.virgolin.gismultipositiontrackerv2.TrackingService.TrackingBinder;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback{

	GoogleApiClient mGoogleApiClient;
	// Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;
	private boolean mRequestingLocationUpdates = false;
    private static final String STATE_RESOLVING_ERROR = "resolving_error";
    private static final String REQUESTING_LOCATION_UPDATES_KEY = "requesting_location_updates";
    private static final String USERNAME_KEY = "username";
    private static Button startTrackingButton;
    private static EditText editUsername;
    private DataHandler dataHandler;
    private LocationRequest mLocationRequest;
    private AlertDialog.Builder alertBuilderEmptyUsernameField;
    private AlertDialog alertEmptyUsername;
    private Thread updateUserPositionOnMapThread;
    private boolean updateUserPositionOnMapThreadFlag = false;
    private GoogleMap map;
    
    Intent trackingService;
    
	private TrackingService mService;
	private boolean mBound;
    
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main);
                
        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);
        
        alertBuilderEmptyUsernameField = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        alertBuilderEmptyUsernameField.setMessage("enter a username to start tracking")
            .setTitle("no username");

     	// 3. Get the AlertDialog from create()
     	alertEmptyUsername = alertBuilderEmptyUsernameField.create();
        
        editUsername = (EditText) findViewById(R.id.editUsername);
        
        startTrackingButton = (Button) findViewById(R.id.btnStartTracking);
        startTrackingButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	startTrackingButtonAction(alertEmptyUsername);
            }
        });
        
        updateValuesFromBundle(savedInstanceState);
        
        trackingService = new Intent(this, TrackingService.class);
        
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        
        mapFragment.getMapAsync(this);
    }
    
    private void updateValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            // Update the value of mRequestingLocationUpdates from the Bundle, and
            // make sure that the Start Updates and Stop Updates buttons are
            // correctly enabled or disabled.
            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        REQUESTING_LOCATION_UPDATES_KEY);
                if(mRequestingLocationUpdates){
                	startTrackingButtonAction(alertEmptyUsername);
                } else {
                	stopTrackingButtonAction(alertEmptyUsername);
                }
            }
            
            if (savedInstanceState.keySet().contains(USERNAME_KEY)){
            	editUsername.setText(savedInstanceState.getString(USERNAME_KEY));
            }
        }
    }
    
    private void startTrackingButtonAction(final Dialog alertEmptyUsername){
    	if(editUsername.getText().toString().isEmpty()){
    		alertEmptyUsername.show();
    	} else {
    		if (!(this.dataHandler != null)){
    			this.dataHandler = new DataHandler(editUsername.getText().toString(), this);
    		}
    		editUsername.setEnabled(false);
    		mRequestingLocationUpdates = true;
    		//this.startLocationUpdates();    		
    		
    		//trackingService.putExtra("username",editUsername.getText().toString());
    		mService.setUsername(editUsername.getText().toString());
    		
    		//this.startService(trackingService);
    		if(mBound){
    			//bindService(trackingService, mConnection, Context.BIND_AUTO_CREATE);
    			mService.setMap(this.map);
    			mService.startTracking();
    			//updateUserPositionOnMap();
    		} else {
    			bindService(trackingService, mConnection, Context.BIND_AUTO_CREATE);    
    			mBound = true;
    			mService.startTracking();
    		}
    		
    		
            startTrackingButton.setText(getResources().getString(R.string.btnStopTracking));
            startTrackingButton.setOnClickListener(new View.OnClickListener() {
            	
                public void onClick(View v) {
                	stopTrackingButtonAction(alertEmptyUsername);
                }
            });
    	}
    }
    
    private void stopTrackingButtonAction(final Dialog alertEmptyUsername){
    	editUsername.setEnabled(true);
    	mRequestingLocationUpdates = false;
    	
    	//clearUserPositionOnMap();
    	//this.stopLocationUpdates();
        
    	if(mBound){
    		mService.stopLocationUpdates();
    		clearMap();
    		//unbindService(mConnection);
            //mBound = false;
    	} else {
    		System.out.println("this line should never be reached");
    	}
    	
    	
    	startTrackingButton.setText(getResources().getString(R.string.btnStartTracking));
        startTrackingButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	startTrackingButtonAction(alertEmptyUsername);
            }
        });
    }
    
    
    
    @Override
    protected void onStart() {
        super.onStart();
        bindService(trackingService, mConnection, Context.BIND_AUTO_CREATE);
        if (!mResolvingError) {  // more about this later
            //mGoogleApiClient.connect();
        }
        
    }
    
    @Override
    protected void onStop() {
        //mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
    	unbindService(mConnection);
    	mBound = false;
    	super.onDestroy();
    };

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/
	
    

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() { }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((MainActivity)getActivity()).onDialogDismissed();
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }
    
    
    
    
    @Override
	protected void onPause() {
	    super.onPause();
	}
	
	public void onSaveInstanceState(Bundle savedInstanceState) {
	    savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY,
	            mRequestingLocationUpdates);
	    savedInstanceState.putString(USERNAME_KEY, editUsername.getText().toString());
	    super.onSaveInstanceState(savedInstanceState);
	    savedInstanceState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);
	}
	
	
	/** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            TrackingBinder binder = (TrackingBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

	@Override
	public void onMapReady(GoogleMap map) {
		this.map = map;
		this.map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
	}
	
	public void clearMap(){
		if(this.map != null){
			try{
				this.map.clear();
			} catch(Exception e){
				
			}
		}
	}
    
	public void addMarker(MarkerOptions mo){
		map.addMarker(mo);
	}
    
	
}
