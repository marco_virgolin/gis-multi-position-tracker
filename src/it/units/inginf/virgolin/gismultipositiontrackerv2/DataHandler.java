package it.units.inginf.virgolin.gismultipositiontrackerv2;

import com.firebase.client.Firebase;

import android.app.Activity;

public class DataHandler extends Activity {
	
	private Firebase firebaseRef;
	private String username;
	private static final String FIREBASE_DB_URL = "https://gismultipostracker.firebaseio.com/";
	
	
	public DataHandler(String username, Activity mainActivity){
		this.username = username;
		Firebase.setAndroidContext(mainActivity);
		firebaseRef = new Firebase(FIREBASE_DB_URL);
	}
	
	public void setUserPosition(double lat, double lon){
		double[] latlon = new double[] { lat, lon };
		firebaseRef.child(this.username).setValue(latlon);
	}
	
}
