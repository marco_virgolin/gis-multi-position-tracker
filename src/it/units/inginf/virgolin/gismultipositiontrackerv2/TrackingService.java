package it.units.inginf.virgolin.gismultipositiontrackerv2;


import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

public class TrackingService extends Service implements LocationListener, ConnectionCallbacks, OnConnectionFailedListener{

	private LocationRequest mLocationRequest;
	private Firebase firebaseRef;
	private String username;
	private static final String FIREBASE_DB_URL = "https://gismultipostracker.firebaseio.com/";
	private GoogleApiClient gac;
	private Notification notification;
	private Location userCurrentLocation;
	private GoogleMap gmap;
	
	private final IBinder mBinder = new TrackingBinder();
	
	public void setMap(GoogleMap gmap){
		this.gmap = gmap;
	}
	
	public class TrackingBinder extends Binder {
		TrackingService getService() {
            // Return this instance of LocalService so clients can call public methods
            return TrackingService.this;
        }
    }
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
	    //TODO do something useful
	    return initializeService(intent, flags, startId);
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public void startTracking(){
		if(!gac.isConnected()){
			gac.connect();
		} else {
			startLocationUpdates();
		}
	}
	
	/*private void updateUserCurrentLatLon(){
		firebaseRef.child(this.username).addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(DataSnapshot snapshot) {
				userCurrentLatLon = (Double[]) snapshot.getValue();  //prints "Do you have data? You'll love Firebase."
			}
			@Override public void onCancelled(FirebaseError error) { }		
		});
	}*/
	
	public Location getUserCurrentLocation(){
		return userCurrentLocation;
	}
	
	private int initializeService(Intent intent, int flags, int startId){
		if(notification == null){
			notification = new Notification.Builder(this)
		    	.setContentTitle("GIS multi position")
		    	.setContentText("tracking your position")
		    	.build();
		}
		
		Firebase.setAndroidContext(this);
		if(firebaseRef == null){
			firebaseRef = new Firebase(FIREBASE_DB_URL);
		}
		if(gac == null){
			gac = new GoogleApiClient.Builder(this)
	    		.addConnectionCallbacks(this)
	    		.addOnConnectionFailedListener(this)
	    		.addApi(LocationServices.API)
	    		.build();
		}
		if(mLocationRequest == null){
			this.createLocationRequest();
		}
		//gac.connect();
		
	    return Service.START_REDELIVER_INTENT;
	}
	
	public void writeUserPositionOnCloud(double lat, double lon){
		double[] latlon = new double[] { lat, lon };
		firebaseRef.child(this.username).setValue(latlon);
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		initializeService(intent, 0, 0);
		return mBinder;
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		if(this.gac.isConnected()){
			this.stopLocationUpdates();
		}
		this.gac.disconnect();
		return true;
	};
	
	@Override
	public void onDestroy(){
		if(this.gac.isConnected()){
			this.stopLocationUpdates();
		}
		this.gac.disconnect();
		super.onDestroy();
	}
	
	protected void startLocationUpdates() {
	    LocationServices.FusedLocationApi.requestLocationUpdates(
	            gac, mLocationRequest, this);
	    startForeground(1, notification);
	}
	
	protected void stopLocationUpdates() {
	    LocationServices.FusedLocationApi.removeLocationUpdates(
	            gac, this);
	    stopForeground(true);
	}
	
	protected void createLocationRequest() {
	    mLocationRequest = new LocationRequest();
	    mLocationRequest.setInterval(10000);
	    mLocationRequest.setFastestInterval(5000);
	    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}

	@Override
	public void onLocationChanged(Location newLocation) {
		this.writeUserPositionOnCloud(newLocation.getLatitude(), newLocation.getLongitude());
		if(gmap != null){
			try{
				gmap.clear();
				gmap.addMarker(new MarkerOptions()
				.title(username)
				.position(new LatLng(newLocation.getLatitude(), newLocation.getLongitude())));
			}
			catch(Exception e){
				
			}
		}
		userCurrentLocation = newLocation;
	}

	

	@Override
	public void onConnectionFailed(ConnectionResult result) {
	}
	
	@Override
	public void onConnected(Bundle arg0) {
		this.startLocationUpdates();
	}



	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		
	}
	

}
