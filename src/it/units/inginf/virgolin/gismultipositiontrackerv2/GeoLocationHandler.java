package it.units.inginf.virgolin.gismultipositiontrackerv2;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import com.google.android.gms.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class GeoLocationHandler extends Activity {
	
	/*private LocationManager locationManager;
	private LocationListener locationListener;*/
	private LocationRequest mLocationRequest;
	private DataHandler dataHandler;
	private GoogleApiClient mGoogleApiClient;
	private LocationListener mainActivity;
	
	private static final int ONE_MINUTE_IN_MS = 60*1000;
	private static final float ONE_HUNDRED_METERS = 100;
	
	
	public GeoLocationHandler(GoogleApiClient mGoogleApiClient, String username, LocationListener mainActivity){
		this.dataHandler = new DataHandler(username, (Activity)mainActivity);
		this.mainActivity = mainActivity;
		this.mGoogleApiClient = mGoogleApiClient;
		/*locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		locationListener = new LocationListener() {
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onLocationChanged(Location location) {
				sendLocationDataToCloud(location);
				
			}
		};*/
	}
	
	/*public void startLocationListener(long minTimeInterval, float minDistanceInterval){
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTimeInterval, minDistanceInterval, locationListener);
	}*/
	
	private void sendLocationDataToCloud(Location location){
		this.dataHandler.setUserPosition(location.getLatitude(), location.getLongitude());
	}

	protected void createLocationRequest() {
	    mLocationRequest = new LocationRequest();
	    mLocationRequest.setInterval(10000);
	    mLocationRequest.setFastestInterval(5000);
	    mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	}
	
	protected void startLocationUpdates() {
	    LocationServices.FusedLocationApi.requestLocationUpdates(
	            mGoogleApiClient, mLocationRequest, mainActivity);
	}

	public void onLocationChanged(Location newLocation) {
		sendLocationDataToCloud(newLocation);		
	}
	
	@Override
	protected void onPause() {
	    super.onPause();
	    stopLocationUpdates();
	}
	
	protected void stopLocationUpdates() {
	    LocationServices.FusedLocationApi.removeLocationUpdates(
	            mGoogleApiClient, mainActivity);
	}
	
}
