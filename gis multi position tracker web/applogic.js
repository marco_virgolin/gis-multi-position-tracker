var usersAndMapElements = [];
var firebase = new Firebase("https://gismultipostracker.firebaseio.com/");
var usedColorCodes = [];
var btnClearData = document.getElementById('btn-clear-data');
var btnExportKML = document.getElementById('btn-export-kml');

function createColor(){
	var colorCode = generateRandomColorCode();
	while(isColorCodeUsed(colorCode)){
		colorCode = generateRandomColorCode();
	}
	return colorCode;
}

function isColorCodeUsed(colorCode){
	for(index in usedColorCodes){
		if (colorCode == usedColorCodes[index])
			return true;
	}
	return false;
}

function generateRandomColorCode(){
	var letters = '0123456789ABCDEF'.split('');
    var code = '#';
    for (var i = 0; i < 6; i++ ) {
        code += letters[Math.floor(Math.random() * 16)];
    }
    return code;
}

// Attach an asynchronous callback to read the data at our posts firebaseerence
firebase.on("value", function(snapshot) {
  var retrievedUsers = snapshot.val();
  for(username in retrievedUsers){
  	if(!doesUserExist(username)){
  		var userAndMapElements = createUserAndMapsElements(username);
  		usersAndMapElements.push(userAndMapElements);
  	}
  	var userAndMapElements = getUserAndMapsElements(username);
  	if(userAndMapElements == null)
  		console.log('error: getUserAndMapsElements('+username+') call returned null');
  	var retrievedLatitude = retrievedUsers[username][0];
  	var retrievedLongitude = retrievedUsers[username][1];
  	var userCurrentPosition = new google.maps.LatLng(retrievedLatitude, retrievedLongitude);
  	userAndMapElements.marker.setPosition(userCurrentPosition);
  	userAndMapElements.polyline.getPath().push(userCurrentPosition);
  }
}, function (errorObject) {
  console.log("The read failed: " + errorObject.code);
});

function createUserAndMapsElements(username){
	var color = createColor();
	var pinImage = new google.maps.MarkerImage("http://www.googlemapsmarkers.com/v1/009900/");
	var infowindow = new google.maps.InfoWindow({
      content: '<div class="info-window"><p>'+username+'</p></div>'
  	});
	var marker = new google.maps.Marker({
    	position: null,
    	title: '#' + username,
    	map: map
  	});
  	google.maps.event.addListener(marker, 'click', function() {
    	infowindow.open(map,marker);
  	});
  	var polylineOptions = {
    	strokeColor: color,
    	strokeOpacity: 1.0,
    	strokeWeight: 3
  	};
  	polyline = new google.maps.Polyline(polylineOptions);
  	polyline.setMap(map);
	var userAndMapElements = {
		username: username,
		marker: marker,
		polyline: polyline,
		infowindow: infowindow
	}
	return userAndMapElements;
}

function getUserAndMapsElements(username){
	for(index in usersAndMapElements){
		if(username == usersAndMapElements[index].username){
			return usersAndMapElements[index];
		}
	}
	return null;
}

function doesUserExist(username){
	for(index in usersAndMapElements){
		if (username == usersAndMapElements[index].username)
			return true;
	}
	return false;
}



btnClearData.onclick = function() {
  var answer = confirm ("Do you really want to delete the recorded data?");
  if (answer){
    clearData();  
  }
}

function clearData(){
  firebase.remove();
  //clear map elements
  for(index in usersAndMapElements){
    usersAndMapElements[index].marker.setMap(null);
    usersAndMapElements[index].polyline.setMap(null);
  }
  //clear arrays
  while(usersAndMapElements.length > 0){
    usersAndMapElements.pop();
    usedColorCodes.pop();
  }
}

btnExportKML.onclick = function(){
  var kmlFile = generateKml();
  var pom = document.createElement('a');
  pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(kmlFile));
  pom.setAttribute('download', "GIS multi position tracker "+new Date()+".kml");
  pom.click();
}

function generateKml(){
  var kmlFile = "<?xml version='1.0' encoding='UTF-8'?><kml xmlns='http://www.opengis.net/kml/2.2'><Document>";
  kmlFile += "<name>GIS multi position tracker recording - "+new Date()+"</name>";
  kmlFile += "<description><![CDATA[]]></description>";

  for(index in usersAndMapElements){
    var ume = usersAndMapElements[index];
    kmlFile += "<Folder>";
    kmlFile += "<name>"+ume.username+"'s track</name>";
    kmlFile += "<Placemark><styleUrl>#marker</styleUrl><name>"+ume.username+"'s last position</name>";
    kmlFile += "<ExtendedData></ExtendedData>";
    kmlFile += "<Point><coordinates>"+ume.marker.position.lng()+","+ume.marker.position.lat()+",0.0</coordinates></Point></Placemark>";
    kmlFile += "<Placemark><styleUrl>#polyline</styleUrl><name>"+ume.username+"'s path</name>";
    kmlFile += "<ExtendedData></ExtendedData>";
    kmlFile += "<LineString><tessellate>1</tessellate>";
    kmlFile += "<coordinates>";

    var polylinePath = ume.polyline.getPath();
    for(jndex in polylinePath.getArray()){
      var polyPosition = polylinePath.getAt(jndex);
      kmlFile += " "+polyPosition.lng()+","+polyPosition.lat()+",0.0"
    }

    kmlFile += "</coordinates></LineString></Placemark>";
    kmlFile += "</Folder>";
  } 
  kmlFile += "</Document></kml>";
  return kmlFile;
}